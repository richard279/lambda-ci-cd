import { APIGatewayProxyResult, APIGatewayEvent } from 'aws-lambda';
import { SQSClient, SendMessageCommand } from '@aws-sdk/client-sqs';

export const handler = async (
  event: APIGatewayEvent,
): Promise<APIGatewayProxyResult> => {
  const client = new SQSClient({ region: 'us-east-1' });
  const command = new SendMessageCommand({
    MessageBody: JSON.stringify({
      id: 21932,
      body: [
        {
          eventId: 119468093,
          subscriptionId: 1683111,
          portalId: 21610591,
          appId: 814342,
          occurredAt: 1663898070000,
          subscriptionType: 'contact.propertyChange',
          attemptNumber: 0,
          objectId: 193901,
          propertyName: 'email',
          propertyValue: 'arqwilliamcoronel@yahoo.com',
          changeSource: 'FORM',
        },
      ],
      Headers: {
        'x-forwarded-for': '54.174.54.29',
        'x-forwarded-proto': 'https',
        'x-forwarded-port': '443',
        host: 'hub-leads-investments-api.3stksscqq1eom.us-west-2.cs.amazonlightsail.com',
        'x-amzn-trace-id': 'Root=1-632d11de-7ef548bb159b5a816a5b9132',
        'content-length': '281',
        'x-hubspot-signature':
          'b2688ddf5c2e34141db6c31793f3e1f45622848c53845afe44134f6d41155cf8',
        'x-hubspot-signature-version': 'v1',
        'x-hubspot-request-timestamp': '1663898078078',
        'x-hubspot-signature-v3':
          'QDBCyOvKC27EXv873ZYgFAgpUDrx9m/aR+NhGztBmkA=',
        'content-type': 'application/json',
        'user-agent':
          'HubSpot Connect 2.0 (http://dev.hubspot.com/) (namespace: webhooks-nio-http-client) - WebhooksExecutorDaemon-executor',
        'x-hubspot-timeout-millis': '10000',
        'x-trace': '',
        accept: '*/*',
      },
      url: '/hub-leads-investments/webhook/capture-hubspot-app-changes',
      method: 'POST',
      userAgent:
        'HubSpot Connect 2.0 (http://dev.hubspot.com/) (namespace: webhooks-nio-http-client) - WebhooksExecutorDaemon-executor',
      ip: '::ffff:172.26.46.156',
    }),
    QueueUrl:
      'https://sqs.us-east-1.amazonaws.com/587401939662/LambdaPrueba.fifo',
    MessageGroupId: '193901',
  });

  try {
    console.log(event);
    const responseSqs = await client.send(command);
    await fetch('https:18.191.194.177:3000/get-lea-from-sqs', {
      method: 'POST',
      body: JSON.stringify(responseSqs),
    });
  } catch (error) {
    console.log({ error });
    return {
      statusCode: 200,
      body: JSON.stringify({
        body: error,
      }),
    };
  }
};
