import { Controller, Get, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return 'Hello World!';
  }

  @Post('get-lead-from-sqs')
  getLead(@Body() body: any): object {
    return this.appService.getLead(JSON.stringify(body));
  }
}
